if 1
    ccc
else
    clear all
end

Condition='Barefeet 1';

flags_imcomplete=[];
imp_picked=importdata('..\data_lfs\subject_data\Processed_data_picked.csv');

%%
% slCharacterEncoding('UTF-8')
cell_add_template=f_read_xml2cell('add_template.osim');
cell_add_template_cut = cell_add_template(1104:4002);
%%
for n=1:length(imp_picked)
    
    temp_cell = strsplit(imp_picked{n},',');
    Subject.name=temp_cell{1};
    Subject.mass=0;
    Subject.height=0;
    Subject.LR=0;
    
    ct_trial=0;
    ind_AB=0;
    
    
    
    ct_rep=0;
    for k=4:6
        ct_rep=ct_rep+1;
        ct_trial=ct_trial+1;
        Lb2_temp=C_modeltool(Subject,Condition,temp_cell{k},0);
        
        %             Lb2_temp.f_make_add_model();
        Lb2_temp.f_load_model();
        Lb2_temp.f_load_res_mot();
        Lb2_temp.f_setup_run_ID(6);
        %                     Lb2_temp.f_load_res_ID();
    end
    
end