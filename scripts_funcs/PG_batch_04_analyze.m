if 1
    ccc
else
    clear all
end

Condition='Barefeet 1';

flags_imcomplete=[];
imp_picked=importdata('..\data_lfs\subject_data\Processed_data_picked.csv');
ct_A=0;
ct_B=0;


if 0
    for n=1:length(imp_picked)
        
        temp_cell = strsplit(imp_picked{n},',');
        Subject.name=temp_cell{1};
        Subject.mass=0;
        Subject.height=0;
        Subject.LR=0;
        
        ct_trial=0;
        ind_AB=0;
        if temp_cell{3}=='A'
            %         figure(1)
            ct_A=ct_A+1;
            ind_AB=1;
            ct_3rd_dim=ct_A;
            
        else
            %         figure(2)
            ct_B=ct_B+1;
            ind_AB=2;
            ct_3rd_dim=ct_B;
        end
        
        
        ct_rep=0;
        for k=4:6
            ct_rep=ct_rep+1;
            ct_trial=ct_trial+1;
            Lb2_temp=C_modeltool(Subject,Condition,temp_cell{k},0);
            Lb2_temp.joints_interest=[Lb2_temp.joints_interest([1,2,4]);...
                [Lb2_temp.joints_interest{4},'_add'];...
                Lb2_temp.joints_interest([5,6])];
            Lb2_temp.f_get_forceplate_from_extloadxml();
            Lb2_temp.f_get_time_Gtrim();
            Lb2_temp.f_get_Gtrim_indx_for_res();
            
            
            
            
            %         Lb2_temp.f_fig_subplot_res_Knee_child(1); %flag_GRF_event
            
            %         Lb2_temp.f_fig_subplot_T_interest_ID_stack(0,'k');
            res_set_Gcontact_temp=Lb2_temp.f_pickout_res_Gcontact();
            %====================
            res_set_Gcontact_normed=res_set_Gcontact_temp;
            
            xq=0:0.01:1;
            x=linspace(0,1,length(Lb2_temp.indx_contact));
            
            res_set_Gcontact_normed.time=interp1(x,res_set_Gcontact_temp.time,xq)/Lb2_temp.Subj.mass;
            res_set_Gcontact_normed.res_ID=interp1(x,res_set_Gcontact_temp.res_ID,xq)/Lb2_temp.Subj.mass;
            res_set_Gcontact_normed.res_SO_act=interp1(x,res_set_Gcontact_temp.res_SO_act,xq);
            res_set_Gcontact_normed.res_SO_force=interp1(x,res_set_Gcontact_temp.res_SO_force,xq)/Lb2_temp.Subj.mass;
            res_set_Gcontact_normed.res_knee_forces=interp1(x,res_set_Gcontact_temp.res_knee_forces,xq)/Lb2_temp.Subj.mass;
            
            SET_ID(ind_AB,ct_rep,ct_3rd_dim,:,:)=res_set_Gcontact_normed.res_ID;
            SET_SO_act(ind_AB,ct_rep,ct_3rd_dim,:,:)=res_set_Gcontact_normed.res_SO_act;
            SET_SO_force(ind_AB,ct_rep,ct_3rd_dim,:,:)=res_set_Gcontact_normed.res_SO_force;
            SET_knee_force(ind_AB,ct_rep,ct_3rd_dim,:,:)=res_set_Gcontact_normed.res_knee_forces;
            
        end
        
    end
else
%     load('SET_data_ID_SO_Knee_BWnorm.mat');
    load('SET_data_ID_SO_Knee_add_BWnorm.mat');
end
%%
[m_ID_1,m_ID_2]=f_sqz_mean(SET_ID,1,[-1.5,0.7],res_set_Gcontact_normed.res_ID_hearder);

% [m_ID_1,m_ID_2]=f_sqz_mean(SET_SO_act,1);
[m_ID_1,m_ID_2]=f_sqz_mean(SET_knee_force,1,[-30,2], res_set_Gcontact_temp.res_knee_header);
%%
[m_ID_1,m_ID_2]=f_sqz_mean_knee(SET_knee_force,1,[-30,2], res_set_Gcontact_temp.res_knee_header);


