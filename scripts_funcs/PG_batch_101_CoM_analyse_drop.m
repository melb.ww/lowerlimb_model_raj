ccc
% import org.opensim.modeling.*;
%
%  SCALEtool=ScaleTool('C:\Users\wuw4\Documents\GIT_local\lowerlimb_model_shark_GUI\data_lfs\subject_data\SHARK068\Barefeet_1\setup_Scale_1.xml');
%
%  SCALEtool.run()

% imp=importdata('..\data_lfs\subject_data\Ben_picked_trial_forceplate.csv');
imp_GRF=importdata('..\docs\Subject_details.xlsx');
%%
tic
ct=0;

ct_error=0;

% name_set=cell(size(imp.data,1),2);
name_set=imp_GRF.textdata(2:end,2);
SetCondition={'barefoot','gel_20','kayano','NB_prof_ff2','NB_sup_ff'};
for n_condition=[1 2 3 4 5]
%     for n_condition=4
    ct_trial=0;
    ct_max=0;
    ct_shear=0;
    for n_subject=[1:5]
        
        
        Condition=SetCondition{n_condition};
        
        Subject.name=name_set{n_subject};
        ct=ct+1;
        Subject.mass=0;
        Subject.height=0;
        Subject.LR=0;
        
        
%         C_modeltool.f_S_repo_C3D(Subject,Condition);
        name_set_c3d=C_modeltool.f_S_repo_C3D_trials(Subject,Condition); % name_set{3}(10:16)\
        
        name_set_c3d_picked_jump=name_set_c3d(contains(name_set_c3d,'jump'))';
%         name_set_c3d_picked=name_set_c3d_picked_jump(contains(name_set_c3d_picked_jump,'0_'));
%         name_set_c3d_picked=name_set_c3d_picked_jump(contains(name_set_c3d_picked_jump,'m45_'));
%         name_set_c3d_picked=name_set_c3d_picked_jump(contains(name_set_c3d_picked_jump,'l45_'));
       name_set_c3d_picked=name_set_c3d_picked_jump(contains(name_set_c3d_picked_jump,'drop_'));
%          name_set_c3d_picked=name_set_c3d_picked_jump(contains(name_set_c3d_picked_jump,'v_'));
        for k=1:length(name_set_c3d_picked)

            [~,temp_trial,~]=fileparts(name_set_c3d_picked{k});
            Lb2=C_modeltool(Subject,Condition,temp_trial,0);
            
%             Lb2.f_fig_subplot_T_interest_ID(0);
            
            %                 Lb2.f_fig_subplot_Knee
            imp_GRF=importdata(Lb2.file_GRF_Gtrim);
            imp_GRF.data_resample=interp1( linspace(0,100,size(imp_GRF.data,1)) ,imp_GRF.data,linspace(0,100,size(imp_GRF.data,1)/10));
            imp_title=split(imp_GRF.textdata{9, 1});
            
            
            if 0
                Lb2.setup_forceplate_LR={{'r','2'},{'r','3'}};
            else
                Lb2.f_setup_forceplate_LR_from_c3dname();
            end
            %%
            if ~exist( Lb2.folder_res_CoM,'dir')
                Lb2.f_setup_run_CoM();
            end
            Lb2.f_load_res_CoM();
            
            
            
            [Knee_axil_moment, knee_angle]=Lb2.f_get_r_knee_axial_moment();
            Knee_shear=Lb2.f_get_knee_shear_force();

            
            %
            %%
            %             a=Lb2.setup_forceplate_LR;
            for n=1:length(Lb2.setup_forceplate_LR)
                n_plate=Lb2.setup_forceplate_LR{n}{2};
                
                
                indx_p=contains(imp_title,n_plate)&contains(imp_title,'_p');
                p_xyz=imp_GRF.data_resample(:,indx_p);
                
                
                indx_v=contains(imp_title,n_plate)&contains(imp_title,'_v');
                f_xyz=imp_GRF.data_resample(:,indx_v);
                f_all=(sum(f_xyz.*f_xyz,2)).^0.5;
                
                indx_f30=(f_all>=50);
                ind_land=find(diff(indx_f30)==1);
                indx_f30(1:ind_land)=0;
                
                if indx_f30(end)==1
                    continue;
                end
                
                p_xy_pick=p_xyz(indx_f30,[1 2 3]);
                f_picked=f_all(indx_f30)/Lb2.Subj.mass/9.8;
                f_picked_max=max(f_picked);
                
                m_picked_knee=Knee_axil_moment(indx_f30);
                angle_picked_knee=knee_angle(indx_f30);
                Knee_shear_picked=Knee_shear(indx_f30,:);
                
                if max(Knee_shear_picked(:,1))>5e4
                   continue
                end
%                 if min(Knee_shear_picked(:,1))<-250
%                    continue
%                 end                
                
%                 figure(n_condition);
%                 subplot(2,1,1)
%                 plot(imp.data_resample(1:length(Knee_shear_picked)),Knee_shear_picked(:,1))
%                 xlabel('Time')
%                 ylabel('AP shear force (N)')
%                 hold on
%                 subplot(2,1,2)
%                 plot(imp.data_resample(1:length(Knee_shear_picked)),Knee_shear_picked(:,2))
%                 hold on
%                 xlabel('Time')
%                 ylabel('ML shear force (N)')
%                 
%                 figure(6)
                figure(n_condition);
                subplot(2,1,1)
                plot(imp_GRF.data_resample(1:length(Knee_shear_picked)), Knee_shear_picked(:,1))
                hold on
                xlabel('Knee angle (deg)')
                ylabel('AP shear force (N)')
                subplot(2,1,2)
                plot(imp_GRF.data_resample(1:length(Knee_shear_picked)), Knee_shear_picked(:,2))
                hold on
                
                
                %============
                ct_shear=ct_shear+1;
%                 [M,I] = min(Knee_shear_picked(:,2));
                [M,I] = min(Knee_shear_picked(1:40,2));
                F_shear_max_all(n_condition,ct_shear)=M/Lb2.Subj.mass/9.8;
                F_shear_max_all_ind(n_condition,ct_shear)=I;
                %============
                
                
                
                
                
                p_com_xyz_pick=Lb2.p_com_xyz(indx_f30,[1 2 3]);
                
                %%
                v=p_com_xyz_pick-p_xy_pick;
                
                v_cos=dot(v,repmat( [0,1,0],size(v,1),1),2)./  ( (dot(v,v,2)).^0.5    );
                
                v_deg=acosd(v_cos);
                
                ct_max=ct_max+1;
                grf_max_all(n_condition,ct_max)=f_picked_max;
                
                
%                 
%                 if length(v_deg)<99
%                     continue;
%                 end
                
                
                
                if 0
                    figure;
                    plot(imp_GRF.data_resample(1:length(v_deg)),v_deg)
                    ylim([0 12])
                    xlabel('Time (s)')
                    ylabel('COP-COM Seperatoin Angle (Deg)')
                    title('Typical profile of the COP-COM Seperatoin Angle during landing')
                    
                    figure;
                    plot(imp_GRF.data_resample(1:length(f_picked)),f_picked)
                    ylim([0 3])
                    xlabel('Time (s)')
                    ylabel('Ground Reaction Force (BW)')
                    title('Typical profile of the Ground Reaction Force during landing')   
                    
                    %=================================

                    h=figure;
                    %                                 plot(v_deg)
                    plot (imp_GRF.data_resample(1:length(v_deg)),v_deg,'k');
                    xlim([-0.08, 1.4])
                    hold on
                    plot([0 0], [0 20], 'k--')
                    plot([0.8 0.8], [0 20], 'k--')
                    
                    xlabel('Time (s)')
                    ylabel('CoM-CoP Seperatoin Angle (Deg)')
                    title('Typical profile of the CoM-CoP Seperatoin Angle during landing')
                    fig_save_pdf_simple(h, 'COMCOP profile')
                end

                %                                 hold on
                %                                 plot(m_picked_knee)
                %                 plot(v_deg(1:99))
                
                %
                %
                %                 hold on;
                %                 title([Condition],'Interpreter','None');
                %                 ylim([-10,10])
                ct_trial=ct_trial+1;
%                 angle_all(n_condition,ct_trial,:)=v_deg(1:99);
                grf_all(n_condition,ct_trial,:)=f_picked(1:40);
                
                
                %                     title(['subject =', num2str(n_subject ), '  ','condition =', num2str(n_condition ), '  ','k =', num2str(k)]);
                %                     plot(v_cos)
                %                     hold on;
                
                %                     subplot(2,1,2)
                
                %% 
                if 0 %% norm AP ML
                    temp_LR=Lb2.setup_forceplate_LR{n}{1};
                    
                    if temp_LR=='r'
                        LR_marker='Right_';
                    else
                        LR_marker='Left_';
                    end
                    TOE=get_marker_from_trc(imp_trc,[LR_marker ,'Foot_toe']);
                    HEEL=get_marker_from_trc(imp_trc,[LR_marker ,'Foot_heel']);
                    v_y=TOE.data(end,[1,3])-HEEL.data(end,[1,3]);
                    v_y_n=v_y/norm(v_y);
                    
                    
                    v_x_n_3v=[v_y_n(2),0,-v_y_n(1)];
                    v_y_n_3v=[v_y_n(1),0,v_y_n(2)];
                    
                    v_proj_y=dot(v,repmat(v_y_n_3v,size(v,1),1),2);
                    v_proj_x=dot(v,repmat(v_x_n_3v,size(v,1),1),2);
                    
                    
                    figure(2)
                    subplot(3,1,1)
                    plot(v_proj_x)
                    hold on
                    subplot(3,1,2)
                    plot(v_proj_y)
                    hold on
                    subplot(3,1,3)
                    scatter(v_proj_x,v_proj_y);
                    hold on
                end
                
                %%
                
                
            end
            
            
            if 0
                imp_trc=read_trcFile(Lb2.file_trc_rot_trimmed_Gtrimmed);
                if 0  %=== Export trc
                    
                    if 1
                        Lb2.f_writeTRCGRF_OS(1);%0 no rotate; 1 flag_rot90
                        Lb2.f_trim_mot_by_GRF()
                        Lb2.f_setup_IK_API(0);
                    end
                    
                    
                    Lb2.f_setup_ExtLoads(0);%flag_winopen_extLoadxml
                    if 1
                        Lb2.f_load_model();
                        Lb2.f_load_res_mot();
                        Lb2.f_setup_run_ID(6);
                        Lb2.f_load_res_ID();
                    end
                    
                    Lb2.f_load_res_ID_interest();
                else
                    
                end
                %                 if (max(abs(Lb2.T_interest_ID.data_norm(:,5)))>2.1)||(max(abs(Lb2.T_interest_ID.data_norm(:,5)))<0.4)
                if 0
                    Lb2.setup_forceplate_LR={{'r','2'},{'r','3'}};
                else
                    Lb2.f_setup_forceplate_LR_from_c3dname();
                end
                
                
                
                if 1
                    Lb2.f_setup_reserve_actuator();
                else
                    Lb2.file_setup_reserve=f_get_path_same_level('data_lfs\dependent\SO_Actuator_PG2_add_bothlegs.xml');
                end
                % Setup SO
                Lb2.f_setup_run_SO_GUI();
                
                % GRF $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
                Lb2.f_setup_run_GRF_GUI(0);% Expressed in: 0 Child, 1 Ground;
                Lb2.f_setup_run_GRF_GUI(1);% Expressed in: 0 Child, 1 Ground;
                Lb2.f_makefile_Knee_force_GUI();

                
            end
        end
    end
    
%     temp_angle=squeeze( angle_all(n_condition,:,:));
    temp_grf=squeeze( grf_all(n_condition,:,:));
    
    temp_indx=~(temp_grf(:,1)==0);
%     temp_angle_nozero=temp_angle(temp_indx,:);
    temp_grf_nozero=temp_grf(temp_indx,:);
    
    
%     angle_mean_all(n_condition,:)=mean(temp_angle_nozero);
    grf_mean_all(n_condition,:)=mean(temp_grf_nozero);
    
    temp_indx_shear=~(F_shear_max_all(n_condition,:)==0);
%     F+_
    f_shear_mean_all(n_condition)=mean(F_shear_max_all(n_condition,temp_indx_shear));
    
    1;
    
end
f_shear_mean_all=-f_shear_mean_all;
toc
angle_mean_all=angle_mean_all';
grf_mean_all=grf_mean_all';
grf_max_all=grf_max_all';
%%
figure
plot(angle_mean_all)
legend(SetCondition,'Interpreter','None')

bar_angle_all=mean(angle_mean_all);
bar_grf_all=mean(grf_mean_all);

bar_grf_max_all=mean(grf_max_all);