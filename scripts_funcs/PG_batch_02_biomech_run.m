ccc
% import org.opensim.modeling.*;
%
%  SCALEtool=ScaleTool('C:\Users\wuw4\Documents\GIT_local\lowerlimb_model_shark_GUI\data_lfs\subject_data\SHARK068\Barefeet_1\setup_Scale_1.xml');
%
%  SCALEtool.run()

% imp=importdata('..\data_lfs\subject_data\Ben_picked_trial_forceplate.csv');
imp=importdata('..\docs\Subject_details.xlsx');
%%
tic
ct=0;

name_set=cell(size(imp.data,1),2);
name_set=imp.textdata(2:end,2);
SetCondition={'barefoot','gel_20','kayano','NB_prof_ff2','NB_sup_ff'};
Condition=SetCondition{1};
for n=1
    %     try
    
    Subject.name=name_set{n};
    ct=ct+1;
    Subject.mass=0;
    Subject.height=0;
    Subject.LR=0;
    
    
    C_modeltool.f_S_repo_C3D(Subject,Condition);
    name_set_c3d=C_modeltool.f_S_repo_C3D_trials(Subject,Condition); % name_set{3}(10:16)
    for k=5%1:12
        %             try
        %                 temp_trial=name_set_c3d{k}(10:16);
        [~,temp_trial,~]=fileparts(name_set_c3d{k});
        Lb2=C_modeltool(Subject,Condition,temp_trial,1);
        %                 Lb2.joints_interest={...
        %                         'hip_flexion_l';...
        %                         'hip_adduction_l';...
        %                         'hip_rotation_l';...
        %                         'knee_angle_l';...
        %                         'ankle_angle_l';...
        %                         'subtalar_angle_l';...
        %                         'hip_flexion_r';...
        %                         'hip_adduction_r';...
        %                         'hip_rotation_r';...
        %                         'knee_angle_r';...
        %                         'ankle_angle_r';...
        %                         'subtalar_angle_r';...
        %                         };
        
        %=== Export trc
        
        if 0
            Lb2.f_writeTRCGRF_OS(1);%0 no rotate; 1 flag_rot90
            
            if 0
                Lb2.setup_forceplate_LR={{'r','2'},{'r','3'}};
            else
                Lb2.f_setup_forceplate_LR_from_c3dname();
            end
            
            Lb2.f_trim_mot_by_GRF()
            Lb2.f_setup_IK_API(0);
        end
        
        Lb2.f_setup_ExtLoads(1);%flag_winopen_extLoadxml
        if 1
            Lb2.f_load_model();
            Lb2.f_load_res_mot();
            Lb2.f_setup_run_ID(6);
            Lb2.f_load_res_ID();
        end
        
        Lb2.f_load_res_ID_interest();
        
        %                 if (max(abs(Lb2.T_interest_ID.data_norm(:,5)))>2.1)||(max(abs(Lb2.T_interest_ID.data_norm(:,5)))<0.4)
        if 0
            warning('Change to force plate 2!')
            
            Lb2.setup_forceplate_LR={{imp.textdata{n,2},'2'}};
            Lb2.f_trim_mot_by_GRF()
            Lb2.f_setup_IK_API(0);
            
            Lb2.f_setup_ExtLoads(0);%flag_winopen_extLoadxml
            if 1
                Lb2.f_load_model();
                Lb2.f_load_res_mot();
                Lb2.f_setup_run_ID(6);
                Lb2.f_load_res_ID();
            end
            Lb2.f_load_res_ID_interest();
        end
        if 1
            Lb2.f_setup_reserve_actuator();
        else
            Lb2.file_setup_reserve=f_get_path_same_level('data_lfs\dependent\SO_Actuator_PG2_add_bothlegs.xml');
        end
        % Setup SO
        Lb2.f_setup_run_SO_GUI();
        
        % GRF $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
        Lb2.f_setup_run_GRF_GUI(0);% Expressed in: 0 Child, 1 Ground;
        Lb2.f_setup_run_GRF_GUI(1);% Expressed in: 0 Child, 1 Ground;
        Lb2.f_makefile_Knee_force_GUI();
        
        %             catch ME
        %                 fprintf( ME.message);
        %             end
    end
    
    %     catch
    %
    %     end
    
    
    if 0
        for k=[1,3,5]
            temp_trial=['Trial',sprintf('%02d',imp.data(n,k))];
            Lb2=C_modeltool(Subject,Condition,temp_trial,1);
            Lb2.setup_forceplate_LR={{imp.textdata{n,2},num2str(imp.data(n,k+1))}};
            %=== Export trc
            Lb2.f_writeTRCGRF_OS(1);%0 no rotate; 1 flag_rot90
            
            Lb2.f_trim_mot_by_GRF()
            Lb2.f_setup_IK_API(0);
        end
    end
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    if 0 % copy repo
        folder_nexus='Z:\My Documents\Vicon Nexus Databases\SHARK Trial\SHARK Trial_PROCESSED\SHARK\';
        C_modeltool.f_S_copyNexusC3D_2_Repo(folder_nexus,' ',Subject,Condition);
    end
    
    if 0 % size scale
        C_modeltool.f_S_repo_C3D(Subject,Condition);
        x = input('cali num = ','s');
        
        static_trial=C_modeltool(Subject,Condition,['Cal ',x],1);
        
        static_trial.f_writeTRCGRF_OS(1);
        
        static_trial.f_set_flag_PluginGait(0); %0 barefoot marker; 1 Plugin Gait
        
        static_trial.f_setup_scale();
        static_trial.f_notificatin();
    end
    
    if 0 % Fmax scale
        static_trial=C_modeltool(Subject,Condition,'Cal 01',0);
        static_trial.f_setup_scale_strengh(0);
    end
    
    
end

toc
