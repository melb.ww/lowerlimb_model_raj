function [C] = f_read_xml2cell(filename)
fid = fopen(filename,'r');
i = 1;
tline = fgetl(fid);
C{i} = tline;
while ischar(tline)
    i = i+1;
    tline = fgetl(fid);
    C{i} = tline;
end
fclose(fid);

end

