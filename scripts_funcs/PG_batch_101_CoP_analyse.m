ccc
% import org.opensim.modeling.*;
%
%  SCALEtool=ScaleTool('C:\Users\wuw4\Documents\GIT_local\lowerlimb_model_shark_GUI\data_lfs\subject_data\SHARK068\Barefeet_1\setup_Scale_1.xml');
%
%  SCALEtool.run()

% imp=importdata('..\data_lfs\subject_data\Ben_picked_trial_forceplate.csv');
imp=importdata('..\docs\Subject_details.xlsx');
%%
tic
ct=0;

ct_error=0;
% name_set=cell(size(imp.data,1),2);
name_set=imp.textdata(2:end,2);
SetCondition={'barefoot','gel_20','kayano','NB_prof_ff2','NB_sup_ff'};
for n_subject=[1:5]
    for n_condition=[1 2 3 4 5]

        Condition=SetCondition{n_condition};

        Subject.name=name_set{n_subject};
        ct=ct+1;
        Subject.mass=0;
        Subject.height=0;
        Subject.LR=0;
        
        
        C_modeltool.f_S_repo_C3D(Subject,Condition);
        name_set_c3d=C_modeltool.f_S_repo_C3D_trials(Subject,Condition); % name_set{3}(10:16)\
        
        name_set_c3d_picked_jump=name_set_c3d(contains(name_set_c3d,'jump'))';
        name_set_c3d_picked=name_set_c3d_picked_jump(contains(name_set_c3d_picked_jump,'0_'));
        for k=1:length(name_set_c3d_picked)
            try

                
                [~,temp_trial,~]=fileparts(name_set_c3d_picked{k});
                Lb2=C_modeltool(Subject,Condition,temp_trial,1);
                
%                 Lb2.f_fig_subplot_Knee
                imp=importdata(Lb2.file_GRF_Gtrim);
                imp.data_resample=interp1( linspace(0,100,size(imp.data,1)) ,imp.data,linspace(0,100,size(imp.data,1)/10));
                imp_title=split(imp.textdata{9, 1});
                
                
                if 0
                    Lb2.setup_forceplate_LR={{'r','2'},{'r','3'}};
                else
                    Lb2.f_setup_forceplate_LR_from_c3dname();
                end
                %%
                if ~exist( Lb2.folder_res_CoM,'dir')
                    Lb2.f_setup_run_CoM();
                end
                Lb2.f_load_res_CoM();
                
                
                imp_trc=read_trcFile(Lb2.file_trc_rot_trimmed_Gtrimmed);

                
                %
                %%
                %             a=Lb2.setup_forceplate_LR;
                for n=1:length(Lb2.setup_forceplate_LR)
                    n_plate=Lb2.setup_forceplate_LR{n}{2};
                    

                    indx_p=contains(imp_title,n_plate)&contains(imp_title,'_p');
                    p_xyz=imp.data_resample(:,indx_p);
                    
                    
                    indx_v=contains(imp_title,n_plate)&contains(imp_title,'_v');
                    f_xyz=imp.data_resample(:,indx_v);
                    f_all=(sum(f_xyz.*f_xyz,2)).^0.5;
                    indx_f30=(f_all>=50);
                    
                    if indx_f30(end)==0
                        continue;
                    end
                    
                    p_xy_pick=p_xyz(indx_f30,[1 2 3]);
                    
                    
%                     p_com_xyz_resample = interp1( linspace(0,100,size(Lb2.p_com_xyz,1)) ,Lb2.p_com_xyz,linspace(0,100,size(p_xyz,1)));
%                     p_com_xyz_resample = interp1( linspace(0,100,size(Lb2.p_com_xyz,1)) ,Lb2.p_com_xyz,linspace(0,100,size(p_xyz,1)));
                    %%
                    
                    p_com_xyz_pick=Lb2.p_com_xyz(indx_f30,[1 2 3]);
                    
                    %%
                    v=p_com_xyz_pick-p_xy_pick;
                    
                    v_cos=dot(v,repmat( [0,1,0],size(v,1),1),2)./  ( (dot(v,v,2)).^0.5    );
                    
                    v_deg=acosd(v_cos);
                    
                    figure(n_condition)
%                     subplot(2,1,1)
                    plot(v_deg)
                    hold on;
                    title([Condition],'Interpreter','None');
                    ylim([-1,])
                    
                    %                     title(['subject =', num2str(n_subject ), '  ','condition =', num2str(n_condition ), '  ','k =', num2str(k)]);
                    
                    
                    plot(v_cos)
                    hold on;
                    
                    subplot(2,1,2)

                    %%
                    
                    if 1 %% norm AP ML
                        temp_LR=Lb2.setup_forceplate_LR{n}{1};
                        
                        if temp_LR=='r'
                            LR_marker='Right_';
                        else
                            LR_marker='Left_';
                        end
                        TOE=get_marker_from_trc(imp_trc,[LR_marker ,'Foot_toe']);
                        HEEL=get_marker_from_trc(imp_trc,[LR_marker ,'Foot_heel']);
                        v_y=TOE.data(end,[1,3])-HEEL.data(end,[1,3]);
                        v_y_n=v_y/norm(v_y);
                        
                    end
                    v_x_n_3v=[v_y_n(2),0,-v_y_n(1)];
                    v_y_n_3v=[v_y_n(1),0,v_y_n(2)];
                    
                    v_proj_y=dot(v,repmat(v_y_n_3v,size(v,1),1),2);
                    v_proj_x=dot(v,repmat(v_x_n_3v,size(v,1),1),2);
                    
                    
                    figure(2)
                    subplot(3,1,1)                    
                    plot(v_proj_x)
                    hold on
                    subplot(3,1,2)
                    plot(v_proj_y)
                    hold on
                    subplot(3,1,3)
                    scatter(v_proj_x,v_proj_y);
                    hold on
                    1
                    
                    %%
                    
                    
                end

                
                if 0
                    if 0  %=== Export trc
                        
                        if 1
                            Lb2.f_writeTRCGRF_OS(1);%0 no rotate; 1 flag_rot90
                            Lb2.f_trim_mot_by_GRF()
                            Lb2.f_setup_IK_API(0);
                        end
                        
                        
                        Lb2.f_setup_ExtLoads(0);%flag_winopen_extLoadxml
                        if 1
                            Lb2.f_load_model();
                            Lb2.f_load_res_mot();
                            Lb2.f_setup_run_ID(6);
                            Lb2.f_load_res_ID();
                        end
                        
                        Lb2.f_load_res_ID_interest();
                    else
                        
                    end
                    %                 if (max(abs(Lb2.T_interest_ID.data_norm(:,5)))>2.1)||(max(abs(Lb2.T_interest_ID.data_norm(:,5)))<0.4)
                    if 0
                        Lb2.setup_forceplate_LR={{'r','2'},{'r','3'}};
                    else
                        Lb2.f_setup_forceplate_LR_from_c3dname();
                    end
                    
                    
                    
                    if 1
                        Lb2.f_setup_reserve_actuator();
                    else
                        Lb2.file_setup_reserve=f_get_path_same_level('data_lfs\dependent\SO_Actuator_PG2_add_bothlegs.xml');
                    end
                    % Setup SO
                    Lb2.f_setup_run_SO_GUI();
                    
                    % GRF $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
                    Lb2.f_setup_run_GRF_GUI(0);% Expressed in: 0 Child, 1 Ground;
                    Lb2.f_setup_run_GRF_GUI(1);% Expressed in: 0 Child, 1 Ground;
                    Lb2.f_makefile_Knee_force_GUI();
                    
                    %             catch ME
                    %                 ct_error=ct_error+1;
                    %                 disp([num2str(ct_error),' =======']);
                    %                 disp(['n_subject =', num2str(n_subject )])%n_subject n_condition k
                    %                 disp(['n_condition =', num2str(n_condition )])
                    %                 disp(['k =', num2str(k)])
                    %
                    %                 fileID = fopen(['Err ',num2str(ct_error),'.txt'],'w');
                    % %                 fileID = fopen(['temp_pause.txt'],'w');
                    %                 fprintf(fileID,['n_subject =', num2str(n_subject ), '  ']);
                    %                 fprintf(fileID,['n_condition =', num2str(n_condition ), '  ']);
                    %                 fprintf(fileID,['k =', num2str(k), '  ']);
                    %                 fprintf(fileID,ME.message);
                    %                 fclose(fileID);
                    % %                                  1
                    %                 %                 fprintf( ME.message);
                    %
                    %             end
                end
            catch
                ct_error=ct_error+1;
                disp([num2str(ct_error),' =======']);
                disp(['n_subject =', num2str(n_subject )])%n_subject n_condition k
                disp(['n_condition =', num2str(n_condition )])
                disp(['k =', num2str(k)])
                
                fileID = fopen(['Err ',num2str(ct_error),'.txt'],'w');
                %                 fileID = fopen(['temp_pause.txt'],'w');
                fprintf(fileID,['n_subject =', num2str(n_subject ), '  ']);
                fprintf(fileID,['n_condition =', num2str(n_condition ), '  ']);
                fprintf(fileID,['k =', num2str(k), '  ']);
                fprintf(fileID,ME.message);
                fclose(fileID);
            end
        end
        
        %         catch
        %
        %         end
    end
end
toc
%%
if 0
    Lb2.f_GUI_c3d;
    Lb2.f_GUI_mot;
    Lb2.f_fig_subplot_res_OpenSim_SO_Fm(0,0); %F
    Lb2.f_fig_subplot_res_OpenSim_SO_Fm(1,0); %a
    Lb2.f_fig_subplot_res_Knee_child(0);
    Lb2.f_fig_subplot_T_interest_ID(0);
    %     Lb2.f_fig_subplot_res_OpenSim_SO_a(0,0);
end
