ccc
% import org.opensim.modeling.*;
%
%  SCALEtool=ScaleTool('C:\Users\wuw4\Documents\GIT_local\lowerlimb_model_shark_GUI\data_lfs\subject_data\SHARK068\Barefeet_1\setup_Scale_1.xml');
%
%  SCALEtool.run()

% imp=importdata('..\data_lfs\subject_data\Ben_picked_trial_forceplate.csv');
imp=importdata('..\docs\Subject_details.xlsx');
name_set=imp.textdata(2:end,2);
%%
tic
ct=0;

ct_error=0;
% name_set=cell(size(imp.data,1),2);
% name_set=imp.textdata(2:end,2);
SetCondition={'barefoot','gel_20','kayano','NB_prof_ff2','NB_sup_ff'};
import org.opensim.modeling.*

mat_bad_n_subject=[];
mat_bad_n_condition=[];
mat_bad_n_musc=[];
mat_bad_max_force=[];
cell_bad_model=cell(0);
for n_subject=[1:5]
    for n_condition=[1 2 3 4 5]
        
        Condition=SetCondition{n_condition};
        
        Subject.name=name_set{n_subject};
        ct=ct+1;
        Subject.mass=0;
        Subject.height=0;
        Subject.LR=0;
        
        
        C_modeltool.f_S_repo_C3D(Subject,Condition);
        name_set_c3d=C_modeltool.f_S_repo_C3D_trials(Subject,Condition); % name_set{3}(10:16)\
        
        name_set_c3d_picked_jump=name_set_c3d(contains(name_set_c3d,'jump'))';
        name_set_c3d_picked=name_set_c3d_picked_jump(contains(name_set_c3d_picked_jump,'0_'));
        for k=1%:length(name_set_c3d_picked)
            %             try
            
            
            [~,temp_trial,~]=fileparts(name_set_c3d_picked{k});
            Lb2=C_modeltool(Subject,Condition,temp_trial,1);
            %             Lb2.file_osim_scaled;
            
            
            if 1
                Lb2.f_load_model;
                
                for n_musc=1:Lb2.OS.nMusc
                    m_temp=Lb2.OS.set_musc.get(n_musc-1);
                    if ~m_temp.get_appliesForce()
                        disp('bad');
                        mat_bad_max_force=[mat_bad_max_force, m_temp.get_max_isometric_force];
                        mat_bad_n_subject=[mat_bad_n_subject,n_subject];
                        mat_bad_n_condition=[mat_bad_n_condition,n_condition];
                        mat_bad_n_musc=[mat_bad_n_musc,n_musc];
                        
                        cell_bad_model=[cell_bad_model;Lb2.file_osim_scaled];
                    end
                    
                    
                    %                     fileID = fopen(['Err - 103 -',num2str(ct_error),'.txt'],'w');
                    %                     fprintf(fileID,['n_subject =', num2str(n_subject ), '  ']);
                    %                     fprintf(fileID,['n_condition =', num2str(n_condition ), '  ']);
                    %                     fprintf(fileID,['k =', num2str(k), '  ']);
                    %                     fprintf(fileID,ME.message);
                    %                     fclose(fileID);
                    
                    %                     %                     disp('===============')
                    %                     %                     m_temp.get_max_isometric_force()
                    %                     %                     disp('===============')
                    %                     m_temp_cast = Thelen2003Muscle.safeDownCast(m_temp);
                    %                     m_temp_cast.set_KshapeActive(1000);
                end
                1;
                %                 Lb2.OS.model.print(Lb2.file_osim_scaled_musc_reconfig);
            end
        end
    end
end
toc
%%
ccc
imp=importdata('..\docs\Subject_details.xlsx');
name_set=imp.textdata(2:end,2);
load('Pick_bad_muscles_2.mat');
SetCondition={'barefoot','gel_20','kayano','NB_prof_ff2','NB_sup_ff'};
for n=1:5
    cell_bad_model{n};
    1;
    n_subject=mat_bad_n_subject(n);
    n_condition=mat_bad_n_condition(n);
    
    Condition=SetCondition{n_condition};
    
    Subject.name=name_set{n_subject};
%     ct=ct+1;
    Subject.mass=0;
    Subject.height=0;
    Subject.LR=0;
    
    
    
    name_set_c3d=C_modeltool.f_S_repo_C3D_trials(Subject,Condition); % name_set{3}(10:16)\
    
    
    if 1
        name_set_c3d_picked=name_set_c3d(contains(name_set_c3d,'jump'))';
    else
        
        name_set_c3d_picked_jump=name_set_c3d(contains(name_set_c3d,'jump'))';
        name_set_c3d_picked=name_set_c3d_picked_jump(contains(name_set_c3d_picked_jump,'0_'));
        %         name_set_c3d_picked=name_set_c3d_picked_jump(contains(name_set_c3d_picked_jump,'m45_'));
        %         name_set_c3d_picked=name_set_c3d_picked_jump(contains(name_set_c3d_picked_jump,'l45_'));
        %             name_set_c3d_picked=name_set_c3d_picked_jump(contains(name_set_c3d_picked_jump,'drop_'));
    end
    
    
    
    
    for k=1:length(name_set_c3d_picked)
        [~,temp_trial,~]=fileparts(name_set_c3d_picked{k});
        Lb2=C_modeltool(Subject,Condition,temp_trial,1);
        Lb2.f_setup_forceplate_LR_from_c3dname();
        if 0
            Lb2.f_setup_reserve_actuator();
        else
            Lb2.file_setup_reserve=f_get_path_same_level('data_lfs\dependent\SO_Actuator_PG2_add_bothlegs.xml');
        end
        % Setup SO
        Lb2.f_get_time_Gtrim();
        
%         Lb2.f_setup_run_SO_GUI();
        
        if 1
            % GRF $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
            Lb2.f_setup_run_GRF_GUI(0);% Expressed in: 0 Child, 1 Ground;
            Lb2.f_setup_run_GRF_GUI(1);% Expressed in: 0 Child, 1 Ground;
%             Lb2.f_makefile_Knee_force_GUI();
        end
    end
end


%%
if 0
    Lb2.f_GUI_c3d;
    Lb2.f_GUI_mot;
    Lb2.f_fig_subplot_res_OpenSim_SO_Fm(0,0); %F
    Lb2.f_fig_subplot_res_OpenSim_SO_Fm(1,0); %a
    Lb2.f_fig_subplot_res_Knee_child(0);
    Lb2.f_fig_subplot_T_interest_ID(0);
    %     Lb2.f_fig_subplot_res_OpenSim_SO_a(0,0);
end
