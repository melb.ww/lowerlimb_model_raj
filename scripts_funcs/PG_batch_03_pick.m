if 1
    ccc
else
    clear all
end

Condition='Barefeet 1';
folders_res_subject_set=C_modeltool.f_S_repo_res_Set();
%%
% picked_indx;
% for n=1:length(folders_res_Set)
if 0
    for n=60
        Subject.name=folders_res_subject_set(n).name;
        
        % Subject.mass=790/9.8;
        Subject.mass=0;
        Subject.height=0;
        Subject.LR=0;
        
        folders_res=C_modeltool.f_S_repo_res(Subject,Condition,1);% ~,~,flag_remove_empty
        
        for k=1:length(folders_res)
            Lb2_temp=C_modeltool(Subject,Condition,folders_res(k).name,0);
            Lb2_temp.f_fig_subplot_T_interest_ID_stack_1dof(1,'red',k);
            
            %         Lb2_temp.f_fig_subplot_T_interest_ID_norm(1);
            %             Lb2_temp.f_fig_subplot_res_OpenSim_SO_Fm(1,1);% flag_plot_acti,flag_GRF_event
            % Lb2_temp.f_fig_subplot_res_OpenSim_SO_reserve_force(0,1);% flag_plot_acti,flag_GRF_event
            %             Lb2_temp.f_fig_subplot_res_Knee_child(1); %flag_GRF_event
        end
        
        
        
        
        
        while 1
            get_good_indx=ginput();
            get_good_indx=round(get_good_indx(:,1))';
            if length(get_good_indx)==3
                break
            else
                warning('length has to be 3')
            end
        end
        
%         picked_indx(n,:)=[n,get_good_indx];
        %     picked_indx
        %     1
        %     close all;
    end
    %% picked
else
    flags_imcomplete=[];
    imp_picked_indx=importdata('..\data_lfs\subject_data\Ben_picked_trials_processed.csv');
    % picked_indx;
    for n=1:length(folders_res_subject_set)
        % for n=1:10
        %     n
        Subject.name=folders_res_subject_set(n).name;
        
        % Subject.mass=790/9.8;
        Subject.mass=0;
        Subject.height=0;
        Subject.LR=0;
        
        folders_res=C_modeltool.f_S_repo_res(Subject,Condition,1);% ~,~,flag_remove_empty
        
        %     for k=1:length(folders_res)
        for k=1:3
            
            Lb2_temp=C_modeltool(Subject,Condition,folders_res(imp_picked_indx.data(n,k)).name,0);
            imp_picked_indx.textdata{n,k+3}=folders_res(imp_picked_indx.data(n,k)).name;
            %         Lb2_temp.f_fig_subplot_T_interest_ID_stack_1dof(1,'red',k);
            folder_GRF=Lb2_temp.folder_res_GRF_GUI_ground;
            if ~exist(folder_GRF, 'dir')
                %             disp('Not exist')
                %             disp(folder_GRF)
                flags_imcomplete(n,k)=1;
            else
                flags_imcomplete(n,k)=0;
            end
            
            
        end
        
        %         Lb2_temp.f_fig_subplot_T_interest_ID_norm(1);
        %             Lb2_temp.f_fig_subplot_res_OpenSim_SO_Fm(1,1);% flag_plot_acti,flag_GRF_event
        % Lb2_temp.f_fig_subplot_res_OpenSim_SO_reserve_force(0,1);% flag_plot_acti,flag_GRF_event
        %             Lb2_temp.f_fig_subplot_res_Knee_child(1); %flag_GRF_event
    end
end
%%

t=cell2table(imp_picked_indx.textdata);
writetable(t,'..\data_lfs\subject_data\Processed_data_picked.csv','WriteVariableNames',false);