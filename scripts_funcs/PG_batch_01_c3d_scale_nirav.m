if 1
    ccc
else
    clear all
end
    
% import org.opensim.modeling.*;
%
%  SCALEtool=ScaleTool('C:\Users\wuw4\Documents\GIT_local\lowerlimb_model_shark_GUI\data_lfs\subject_data\SHARK068\Barefeet_1\setup_Scale_1.xml');
%
%  SCALEtool.run()

imp=importdata('..\docs\Subject_details_ARCL.xlsx');
%%
% AB=imp.textdata(:,7);
ct=0;
name_set=cell(size(imp.data,1),2);
% imp_subjdata= importdata('..\data_lfs\subject_data\Shark_dataset_simple.csv');
for n_subj=1%5%[3 5]
    
    Subject.name=imp.textdata{n_subj+1,2};
    ct=ct+1;
    if 1
        
        name_set{ct,1}=imp.textdata{n_subj+1,1};
        %         ind=find(contains(imp_subjdata.textdata,name_set{ct}));
        if imp.data(n_subj,2)==1
            name_set{ct,2}='l';
        else
            name_set{ct,2}='r';
        end
        
        
    end
    Subject.mass=0;
    Subject.height=0;
    Subject.LR=0;
    
%     SetCondition={'barefoot','gel_20','kayano','NB_prof_ff2','NB_sup_ff'};
    SetCondition={'barefoot','AX'};
    for n_condition=2 %=========================
        Condition=SetCondition{n_condition};
        if 0 % copy repo
%             folder_nexus='C:\Users\wuw4\OneDrive - The University of Melbourne\Documents\Unimelb_research\2020_product_testing\data_collected\Asics_product_testing\Pilot\';
            folder_nexus='C:\Users\wuw4\OneDrive - The University of Melbourne\Documents\Unimelb_research\2020_product_testing\data_collected\Asics_product_testing\ARC-L';
            C_modeltool.f_S_copyNexusC3D_2_Repo_AllConditions(folder_nexus,'',Subject);
            1;
        end
        
        if 1 % size scale
            C_modeltool.f_S_repo_C3D(Subject,Condition);
            if 0
                x = input('cali_file= ','s');
                static_trial=C_modeltool(Subject,Condition,[x],1);
            else
                static_trial=C_modeltool(Subject,Condition,'cali',1);
            end
            
            static_trial.f_writeTRCGRF_OS(1);
            
            if n_condition==1
                static_trial.f_set_flag_barefoot(1);
            else
                static_trial.f_set_flag_barefoot(0);
            end
            %         static_trial.f_set_flag_PluginGait(0); %0 barefoot marker; 1 Plugin Gait
            
            static_trial.f_setup_scale();
            x = input(' ... ','s');
%             static_trial.f_notificatin();
        end
        
        if 1 % Fmax scale
            static_trial=C_modeltool(Subject,Condition,'Cal 01',0);
            static_trial.f_setup_scale_strengh(0);
            %         static_trial.f_setup_scale_strengh_temp(6.82);
        end
    end
end


