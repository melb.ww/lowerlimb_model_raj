ccc
% import org.opensim.modeling.*;
%
%  SCALEtool=ScaleTool('C:\Users\wuw4\Documents\GIT_local\lowerlimb_model_shark_GUI\data_lfs\subject_data\SHARK068\Barefeet_1\setup_Scale_1.xml');
%
%  SCALEtool.run()

imp=importdata('..\docs\30Barefoot_SHARK_for_Adam_fromBen.xlsx');
%%
AB=imp.textdata(:,7);
ct=0;
name_set=cell(163,3);
imp_subjdata= importdata('..\data_lfs\subject_data\Shark_dataset_simple.csv');
% for n=52:length(AB)
for n=50
    flag_temp_AB=length(AB{n});
    if ~flag_temp_AB
        Subject.name=imp.textdata{n,6};
        ct=ct+1;
        if 1
            
            name_set{ct,1}=imp.textdata{n,6};
            ind=find(contains(imp_subjdata.textdata,name_set{ct}));  
            if imp_subjdata.data(ind,1)==1
                name_set{ct,2}='l';
            end
            if imp_subjdata.data(ind,1)==2
                name_set{ct,2}='r';
            end
            name_set{ct,3}=AB{n};
            
        end
        Subject.mass=0;
        Subject.height=0;
        Subject.LR=0;
        Condition='Barefeet 1';
        if 0 % copy repo
            folder_nexus='Z:\My Documents\Vicon Nexus Databases\SHARK Trial\SHARK Trial_PROCESSED\SHARK\';
            C_modeltool.f_S_copyNexusC3D_2_Repo(folder_nexus,' ',Subject,Condition);
        end
        
        if 1 % size scale
            C_modeltool.f_S_repo_C3D(Subject,Condition);
            x = input('cali num = ','s');
            
            static_trial=C_modeltool(Subject,Condition,['Cal ',x],1);
            
            static_trial.f_writeTRCGRF_OS(1);
            
%             static_trial.f_set_flag_PluginGait(0); %0 barefoot marker; 1 Plugin Gait
            static_trial.f_set_flag_PluginGait(1); %0 barefoot marker; 1 Plugin Gait
            
            static_trial.f_setup_scale();
            static_trial.f_notificatin();
        end
        
        if 0 % Fmax scale
            static_trial=C_modeltool(Subject,Condition,'Cal 01',0);
            static_trial.f_setup_scale_strengh(0);
        end
        
    end
    
end


