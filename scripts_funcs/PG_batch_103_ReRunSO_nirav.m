ccc
% import org.opensim.modeling.*;
%
%  SCALEtool=ScaleTool('C:\Users\wuw4\Documents\GIT_local\lowerlimb_model_shark_GUI\data_lfs\subject_data\SHARK068\Barefeet_1\setup_Scale_1.xml');
%
%  SCALEtool.run()

% imp=importdata('..\data_lfs\subject_data\Ben_picked_trial_forceplate.csv');
imp=importdata('..\docs\Subject_details.xlsx');
%%
tic
ct=0;

ct_error=0;
% name_set=cell(size(imp.data,1),2);
name_set=imp.textdata(2:end,2);
SetCondition={'barefoot','gel_20','kayano','NB_prof_ff2','NB_sup_ff'};
% for n_subject=[1:5]
for n_subject=1
%     for n_condition=[1:5]
    for n_condition=2:5
        % for n_condition=4
        Condition=SetCondition{n_condition};
        
        %     for n_subject=4
        
        
        
        Subject.name=name_set{n_subject};
        ct=ct+1;
        Subject.mass=0;
        Subject.height=0;
        Subject.LR=0;
        
        
        C_modeltool.f_S_repo_C3D(Subject,Condition);
        name_set_c3d=C_modeltool.f_S_repo_C3D_trials(Subject,Condition); % name_set{3}(10:16)\
        if 0
            name_set_c3d_picked=name_set_c3d(contains(name_set_c3d,'jump'))';
        else
            
            name_set_c3d_picked_jump=name_set_c3d(contains(name_set_c3d,'jump'))';
                    name_set_c3d_picked=name_set_c3d_picked_jump(contains(name_set_c3d_picked_jump,'0_'));
%                     name_set_c3d_picked=name_set_c3d_picked_jump(contains(name_set_c3d_picked_jump,'m45_'));
            %         name_set_c3d_picked=name_set_c3d_picked_jump(contains(name_set_c3d_picked_jump,'l45_'));
%             name_set_c3d_picked=name_set_c3d_picked_jump(contains(name_set_c3d_picked_jump,'drop_'));
        end
        
        for k=1:length(name_set_c3d_picked)

%             try
                [~,temp_trial,~]=fileparts(name_set_c3d_picked{k});
                Lb2=C_modeltool(Subject,Condition,temp_trial,1);
                if 0
                    Lb2.setup_forceplate_LR={{'r','2'},{'r','3'}};
                else
                    Lb2.f_setup_forceplate_LR_from_c3dname();
                end
                if 1 %=== Export trc
                    
                    if 1
%                         Lb2.f_writeTRCGRF_OS(1);%0 no rotate; 1 flag_rot90
                        Lb2.f_trim_mot_by_GRF()
                        Lb2.f_setup_IK_API(0);
                    end
                    
                    
                    Lb2.f_setup_ExtLoads(0);%flag_winopen_extLoadxml
                    if 1
                        Lb2.f_load_model();
                        Lb2.f_load_res_mot();
                        Lb2.f_setup_run_ID(6);
                        Lb2.f_load_res_ID();
                    end
                    
                    Lb2.f_load_res_ID_interest();
                else
                    
                end
                %                 if (max(abs(Lb2.T_interest_ID.data_norm(:,5)))>2.1)||(max(abs(Lb2.T_interest_ID.data_norm(:,5)))<0.4)

                
%                 if 0
%                     Lb2.f_setup_reserve_actuator();
%                 else
%                     Lb2.file_setup_reserve=f_get_path_same_level('data_lfs\dependent\SO_Actuator_PG2_add_bothlegs.xml');
%                 end
                % Setup SO
                Lb2.f_get_time_Gtrim()
                 Lb2.f_setup_run_SO_GUI();
                
                % GRF $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
                Lb2.f_setup_run_GRF_GUI(0);% Expressed in: 0 Child, 1 Ground;
                Lb2.f_setup_run_GRF_GUI(1);% Expressed in: 0 Child, 1 Ground;
                Lb2.f_makefile_Knee_force_GUI();
                1;
%             catch ME
%                 ct_error=ct_error+1;
% %                 disp([num2str(ct_error),' =======']);
% %                 disp(['n_subject =', num2str(n_subject )])%n_subject n_condition k
% %                 disp(['n_condition =', num2str(n_condition )])
% %                 disp(['k =', num2str(k)])
%                 
%                 fileID = fopen(['Err - 103_nirav -',num2str(ct_error),'.txt'],'w');
%                 fprintf(fileID,['n_subject =', num2str(n_subject ), '  ']);
%                 fprintf(fileID,['n_condition =', num2str(n_condition ), '  ']);
%                 fprintf(fileID,['k =', num2str(k), '  ']);
%                 fprintf(fileID,ME.message);
%                 fclose(fileID);
%                 
%             end
        end
    end
end
toc
%%
if 0
    Lb2.f_GUI_c3d;
    Lb2.f_GUI_mot;
    Lb2.f_fig_subplot_res_OpenSim_SO_Fm(0,0); %F
    Lb2.f_fig_subplot_res_OpenSim_SO_Fm(1,0); %a
    Lb2.f_fig_subplot_res_Knee_child(0);
    Lb2.f_fig_subplot_T_interest_ID(0);
    %     Lb2.f_fig_subplot_res_OpenSim_SO_a(0,0);
end
