ccc
imp_GRF=importdata('..\docs\Subject_details.xlsx');
%%
tic
ct=0;

ct_error=0;
ct_fig=0;
% name_set=cell(size(imp.data,1),2);
name_set=imp_GRF.textdata(2:end,2);
SetCondition={'barefoot','gel_20','kayano','NB_prof_ff2','NB_sup_ff'};
for n_condition=[1 2 3 4 5]
    %     for n_condition=4
    ct_trial=0;
    ct_max=0;
    ct_shear=0;
    for n_subject=[1:5]
        
        
        Condition=SetCondition{n_condition};
        
        Subject.name=name_set{n_subject};
        ct=ct+1;
        Subject.mass=0;
        Subject.height=0;
        Subject.LR=0;
        
        name_set_c3d=C_modeltool.f_S_repo_C3D_trials(Subject,Condition); % name_set{3}(10:16)\
        
        name_set_c3d_picked_jump=name_set_c3d(contains(name_set_c3d,'jump'))';
%         name_set_c3d_picked=name_set_c3d_picked_jump(contains(name_set_c3d_picked_jump,'0_'));
                name_set_c3d_picked=name_set_c3d_picked_jump(contains(name_set_c3d_picked_jump,'m45_'));
        %         name_set_c3d_picked=name_set_c3d_picked_jump(contains(name_set_c3d_picked_jump,'l45_'));
        %        name_set_c3d_picked=name_set_c3d_picked_jump(contains(name_set_c3d_picked_jump,'drop_'));
        %          name_set_c3d_picked=name_set_c3d_picked_jump(contains(name_set_c3d_picked_jump,'v_'));
        for k=1:length(name_set_c3d_picked)
            
            [~,temp_trial,~]=fileparts(name_set_c3d_picked{k});
            Lb2=C_modeltool(Subject,Condition,temp_trial,0);
            
            Knee_shear=Lb2.f_get_knee_shear_force();
            
            imp_GRF=importdata(Lb2.file_GRF_Gtrim);
            imp_GRF.data_resample=interp1( linspace(0,100,size(imp_GRF.data,1)) ,imp_GRF.data,linspace(0,100,size(Knee_shear,1)));
            imp_title=split(imp_GRF.textdata{9, 1});
            
            
            if 0
                Lb2.setup_forceplate_LR={{'r','2'},{'r','3'}};
            else
                Lb2.f_setup_forceplate_LR_from_c3dname();
            end
            %%
            
            for n=1:length(Lb2.setup_forceplate_LR)
                n_plate=Lb2.setup_forceplate_LR{n}{2};
                
                
                indx_p=contains(imp_title,n_plate)&contains(imp_title,'_p');
                p_xyz=imp_GRF.data_resample(:,indx_p);
                
                
                indx_v=contains(imp_title,n_plate)&contains(imp_title,'_v');
                f_xyz=imp_GRF.data_resample(:,indx_v);
                f_all=(sum(f_xyz.*f_xyz,2)).^0.5;
                
                indx_f30=(f_all>=40);
                ind_land=find(diff(indx_f30)==1);
                indx_f30(1:ind_land)=0;
                
                
                
                
                
                if indx_f30(end)==0
                    continue;
                end
                
                if 0
                    %                 figure(n_condition+10);
                    figure;
                    plot(imp_GRF.data_resample(1:sum(indx_f30),1),f_xyz(indx_f30,2));
                    hold on;
                end
                
                p_xy_pick=p_xyz(indx_f30,[1 2 3]);
                f_picked=f_all(indx_f30)/Lb2.Subj.mass/9.8;
                f_picked_max=max(f_picked);
                
                indx_pre=find(indx_f30);
                indx_pre=[[indx_pre(1)-20:indx_pre(1)-1]'; indx_pre];
                
                
                Knee_shear_picked=Knee_shear(indx_pre,:);
                
                if max(abs(Knee_shear_picked(:,1)))>3e3
                    1;
                    % continue
                end
                                length_draw=50;
                figure(n_condition);
                plot(imp_GRF.data_resample(1:length_draw,1), -Knee_shear_picked(1:length_draw,1))
                %                 plot(imp_GRF.data_resample(1:length(Knee_shear_picked)), Knee_shear_picked(:,1))
                hold on
                xlabel('Time (s)')
                ylabel('AP shear force (N)')
                
                if 0
                ct_fig=ct_fig+1;

                figure(n_condition);
                subplot(2,1,1)
                plot(imp_GRF.data_resample(1:length_draw,1), Knee_shear_picked(1:length_draw,1))
%                 plot(imp_GRF.data_resample(1:length(Knee_shear_picked)), Knee_shear_picked(:,1))
                hold on
                xlabel('Time (s)')
                ylabel('AP shear force (N)')
                subplot(2,1,2)
                plot(imp_GRF.data_resample(1:length_draw,1), Knee_shear_picked(1:length_draw,2))
                hold on
              
                    %                 if 0
                    figure
                    subplot(2,1,1)
                    plot(imp_GRF.data_resample(:,1), Knee_shear(:,1))
                    hold on
                    xlabel('Time (s)')
                    ylabel('AP shear force (N)')
                    subplot(2,1,2)
                    plot(imp_GRF.data_resample(:,1), Knee_shear(:,2))
                    hold on
                end
                
                
                %============
                ct_shear=ct_shear+1;
                [M,I] = min(Knee_shear_picked(1:49,2));
                F_shear_max_all(n_condition,ct_shear)=M/Lb2.Subj.mass/9.8;
                F_shear_max_all_ind(n_condition,ct_shear)=I;
                
            end
            1;
        end
        1;
        
    end
    1;
    temp_indx_shear=~(F_shear_max_all(n_condition,:)==0);
    f_shear_mean_all(n_condition)=mean(F_shear_max_all(n_condition,temp_indx_shear));
end
f_shear_mean_all=-f_shear_mean_all;
toc