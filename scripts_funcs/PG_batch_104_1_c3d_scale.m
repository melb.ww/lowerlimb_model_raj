if 1
    ccc
else
    clear all
end

imp=importdata('..\docs\Subject_details_ARCL.xlsx');
name_set=imp.textdata(2:end,2);


for n_subject=1
    
    Subject.name=name_set{n_subject};
    Subject.mass=0;
    Subject.height=0;
    Subject.LR=0;
    
    if 0 % copy repo
        %             folder_nexus='C:\Users\wuw4\OneDrive - The University of Melbourne\Documents\Unimelb_research\2020_product_testing\data_collected\Asics_product_testing\Pilot\';
        folder_nexus='C:\Users\wuw4\OneDrive - The University of Melbourne\Documents\Unimelb_research\2020_product_testing\data_collected\Asics_product_testing\ARC-L';
        C_modeltool.f_S_copyNexusC3D_2_Repo_AllConditions(folder_nexus,'',Subject);
        1;
    end
    
    SetCondition={'barefoot','AX'};%C_modeltool.f_S_get_conditions_from_C3D(name_set{n_subject}); %
    
    for n_condition=2 %=========================
        Condition=SetCondition{n_condition};
        
        
        
        if 0 % size scale
            C_modeltool.f_S_repo_C3D(Subject,Condition);
            if 0
                x = input('cali_file= ','s');
                static_trial=C_modeltool(Subject,Condition,[x],1);
            else
                static_trial=C_modeltool(Subject,Condition,'cali',1);
            end
            
            static_trial.f_writeTRCGRF_OS(1);
            
            %             if n_condition==1
            %                 static_trial.f_set_flag_barefoot(1);
            %             else
            %                 static_trial.f_set_flag_barefoot(0);
            %             end
            %         static_trial.f_set_flag_PluginGait(0); %0 barefoot marker; 1 Plugin Gait
            
            static_trial.f_setup_scale();
            x = input(' ... ','s');
            %             static_trial.f_notificatin();
        end
        
        if 0 % Fmax scale
            static_trial=C_modeltool(Subject,Condition,'Cal 01',0);
            static_trial.f_setup_scale_strengh(0);
            %         static_trial.f_setup_scale_strengh_temp(6.82);
        end
        
        if 1 % copy spreadout shod models to each condition
            static_trial=C_modeltool(Subject,Condition,'Cal 01',0);
            
            
            SetCondition_temp=C_modeltool.f_S_get_conditions_from_C3D(name_set{n_subject}); % SetCondition={'barefoot','AX'};
            SetCondition_shod_to=SetCondition_temp(~(contains(SetCondition_temp,'barefoot','IgnoreCase',true)|contains(SetCondition_temp,Condition,'IgnoreCase',true)));
            
            for n_shod_to=1:length(SetCondition_shod_to)
                temp_file=static_trial.file_osim_scaled;
                folder_dest=['..\data_lfs\subject_data\Subject_01\',SetCondition_shod_to{n_shod_to}];
                copyfile(temp_file,folder_dest);
            end
        end        
    end
end




