if 1
    ccc
else
    clear all
end

imp=importdata('..\docs\Subject_details_ARCL.xlsx');
%%
tic
ct_cond=0;
ct_error=0;

name_set=imp.textdata(2:end,2);

for n_subject=1
    
    Subject.name=name_set{n_subject};
    Subject.mass=0;
    Subject.height=0;
    Subject.LR=0;
    
    SetCondition=C_modeltool.f_S_get_conditions_from_C3D(name_set{n_subject}); % SetCondition={'barefoot','AX'};
    
    for n_condition=10%1:length(SetCondition)
        
        Condition=SetCondition{n_condition};
        
        ct_cond=ct_cond+1;
        
        C_modeltool.f_S_repo_C3D(Subject,Condition);
        name_set_c3d=C_modeltool.f_S_repo_C3D_trials(Subject,Condition); % name_set{3}(10:16)\
        
        name_set_c3d_picked=name_set_c3d(contains(name_set_c3d,'run')|contains(name_set_c3d,'drop'))';
        
        for k=2:length(name_set_c3d_picked)
            
            try
                
                [~,temp_trial,~]=fileparts(name_set_c3d_picked{k});
                Lb2=C_modeltool(Subject,Condition,temp_trial,1);
                
                if ~exist(Lb2.file_GRF, 'file')
                    Lb2.f_writeTRCGRF_OS(1);%0 no rotate; 1 flag_rot90
                end
                
                if contains(temp_trial,'drop')
                    Lb2.setup_forceplate_LR={{'r','3'},{'l','1'}};%Lb2.f_setup_forceplate_LR_from_c3dname();
                end
                if contains(temp_trial,'run')
                    Lb2.f_setup_forceplate_from_GRF_single();
                end
                
                if ~exist(Lb2.file_mot, 'file')
                    Lb2.f_trim_mot_by_GRF()
                    Lb2.f_setup_IK_API(0);
                end
                
                
                Lb2.f_setup_ExtLoads(0);%flag_winopen_extLoadxml
                if 1
                    Lb2.f_load_model();
                    Lb2.f_load_res_mot();
                    Lb2.f_setup_run_ID(6);
                    Lb2.f_load_res_ID();
                end
                
                
                if 1
                    %             Lb2.f_get_time_Gtrim()
                    Lb2.f_setup_run_SO_GUI();
                    %
                    % GRF $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
                    Lb2.f_setup_run_GRF_GUI(0);% Expressed in: 0 Child, 1 Ground;
                    Lb2.f_setup_run_GRF_GUI(1);% Expressed in: 0 Child, 1 Ground;
                    Lb2.f_makefile_Knee_force_GUI();
                end
                
                
                Lb2.f_load_MA_LM;
                if isempty(Lb2.res_ID)
                    Lb2.f_load_res_ID();
                end
                Lb2.f_setup_run_KneeLoad_GUI(0.0422);
%                 Lb2.f_fig_subplot_Knee(1);
                1;
                
            catch ME
                ct_error=ct_error+1;
                %                 disp([num2str(ct_error),' =======']);
                %                 disp(['n_subject =', num2str(n_subject )])%n_subject n_condition k
                %                 disp(['n_condition =', num2str(n_condition )])
                %                 disp(['k =', num2str(k)])
                
                fileID = fopen(['Err - 104_1 -',num2str(ct_error),'.txt'],'w');
                fprintf(fileID,['n_subject =', num2str(n_subject ), '  ']);
                fprintf(fileID,['n_condition =', num2str(n_condition ), '  ']);
                fprintf(fileID,['k =', num2str(k), '  ']);
                fprintf(fileID,ME.message);
                fclose(fileID);
                
            end
        end
        1;
    end
end
toc
%%
if 0
    Lb2.f_load_MA_LM;
    
    if isempty(Lb2.res_ID)
        Lb2.f_load_res_ID();
    end
    Lb2.f_setup_run_KneeLoad_GUI(0.0422);
    Lb2.f_fig_subplot_Knee(0);
end

if 0
    Lb2.f_GUI_c3d;
    Lb2.f_GUI_mot;
    
    Lb2.f_fig_plot_GRF();
    
    Lb2.f_fig_subplot_res_OpenSim_SO_Fm(0,0); %F
    Lb2.f_fig_subplot_res_OpenSim_SO_Fm(1,0); %a
    Lb2.f_fig_subplot_res_Knee_child(1);
    Lb2.f_fig_subplot_T_interest_ID(1);
    %     Lb2.f_fig_subplot_res_OpenSim_SO_a(0,0);
end
