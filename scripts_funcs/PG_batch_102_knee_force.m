ccc
% import org.opensim.modeling.*;
%
%  SCALEtool=ScaleTool('C:\Users\wuw4\Documents\GIT_local\lowerlimb_model_shark_GUI\data_lfs\subject_data\SHARK068\Barefeet_1\setup_Scale_1.xml');
%
%  SCALEtool.run()

% imp=importdata('..\data_lfs\subject_data\Ben_picked_trial_forceplate.csv');
imp=importdata('..\docs\Subject_details.xlsx');
%%
tic
ct=0;

ct_error=0;
% name_set=cell(size(imp.data,1),2);
name_set=imp.textdata(2:end,2);
SetCondition={'barefoot','gel_20','kayano','NB_prof_ff2','NB_sup_ff'};
for n_subject=[1:5]
    for n_condition=[1 2 3 4 5]
        % for n_condition=4
        Condition=SetCondition{n_condition};
        
        %     for n_subject=4
        
        
        
        Subject.name=name_set{n_subject};
        ct=ct+1;
        Subject.mass=0;
        Subject.height=0;
        Subject.LR=0;
        
        
        C_modeltool.f_S_repo_C3D(Subject,Condition);
        name_set_c3d=C_modeltool.f_S_repo_C3D_trials(Subject,Condition); % name_set{3}(10:16)\
        
        name_set_c3d_picked_jump=name_set_c3d(contains(name_set_c3d,'jump'))';
        name_set_c3d_picked=name_set_c3d_picked_jump(contains(name_set_c3d_picked_jump,'l45_'));
        for k=1:length(name_set_c3d_picked)
            try
                
                [~,temp_trial,~]=fileparts(name_set_c3d_picked{k});
                Lb2=C_modeltool(Subject,Condition,temp_trial,1);
                Lb2.f_fig_subplot_res_Knee_child(0);
            catch
                ct_error=ct_error+1;
                disp([num2str(ct_error),' =======']);
                disp(['n_subject =', num2str(n_subject )])%n_subject n_condition k
                disp(['n_condition =', num2str(n_condition )])
                disp(['k =', num2str(k)])
                
                fileID = fopen(['Err ',num2str(ct_error),'.txt'],'w');
                %                 fileID = fopen(['temp_pause.txt'],'w');
                fprintf(fileID,['n_subject =', num2str(n_subject ), '  ']);
                fprintf(fileID,['n_condition =', num2str(n_condition ), '  ']);
                fprintf(fileID,['k =', num2str(k), '  ']);
                fprintf(fileID,ME.message);
                fclose(fileID);
            end
        end
        
        %         catch
        %
        %         end
    end
end
toc
%%
if 0
    Lb2.f_GUI_c3d;
    Lb2.f_GUI_mot;
    Lb2.f_fig_subplot_res_OpenSim_SO_Fm(0,0); %F
    Lb2.f_fig_subplot_res_OpenSim_SO_Fm(1,0); %a
    Lb2.f_fig_subplot_res_Knee_child(0);
    Lb2.f_fig_subplot_T_interest_ID(0);
    %     Lb2.f_fig_subplot_res_OpenSim_SO_a(0,0);
end
