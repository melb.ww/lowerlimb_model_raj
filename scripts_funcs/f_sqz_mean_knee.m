function [mean1,mean2] = f_sqz_mean_knee(SET_ID,flag_plot,y_lim,titleset)
% y_lim=[0,1.1];
a1=squeeze(SET_ID(1,1,:,:,:));
a2=squeeze(SET_ID(1,2,:,:,:));
a3=squeeze(SET_ID(1,3,:,:,:));

SET_ID_stack_1=[a1;a2;a3];

a1=squeeze(SET_ID(2,1,:,:,:));
a2=squeeze(SET_ID(2,2,:,:,:));
a3=squeeze(SET_ID(2,3,:,:,:));

SET_ID_stack_2=[a1;a2;a3];

mean1=squeeze( mean(SET_ID_stack_1,1));
mean2=squeeze( mean(SET_ID_stack_2,1));

std1=squeeze( std(SET_ID_stack_1,1));
std2=squeeze(  std(SET_ID_stack_2,1));
if flag_plot
    figure
    subplot_layout=f_numSubplots(size(mean1,2));
    subplot_layout=[3,1];
    ct=0;
    for n=[2,5,8]
        ct=ct+1;
        subplot(subplot_layout(1),subplot_layout(2),ct)
        plot([0:100],[mean1(:,n)],'k-');
%                 shadedErrorBar([0:100],[mean1(:,n)],std1(:,n),'k-',0.95)
        hold on
        plot([0:100],[mean2(:,n)],'r-');
%                 shadedErrorBar([0:100],[mean2(:,n)],std2(:,n),'r-',0.95)
%         if rem(ct, 2) == 0
%             ylim([-30,2])
%         else
%             ylim([-3,3])
%         end
ylim([-30,2]);
        title(titleset{n},'Interpreter','none')

    end
end
end
